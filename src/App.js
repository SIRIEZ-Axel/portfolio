import About  from "./assets/Components/About";
import Intro from "./assets/Components/Intro";
import './index.css'

function App() {
  return (
    <div className="App">
      <Intro />
      <About />
    </div>
  );
}

export default App;
